import pandas as pd
from matplotlib import pyplot as plt
from statistics import mean
from barplotCreator import createBarplot

episodesDf = pd.read_csv('data/episode_data_wviews.csv')

print("\n5 measure analysis of the ratings: \n")
print(episodesDf[["rating", "ratingCount"]].describe())

print("\n # of NA values: \n")
print(episodesDf.isna().sum())

print("\n The 15 highest rated episodes: \n")
print(episodesDf.nlargest(15, 'rating')[["title", "rating"]])

print("\n The 15 lowest rated episodes: \n")
print(episodesDf.nsmallest(15, 'rating')[["title", "rating"]])

# plotting the imdb rating and the number of imdb ratings of each episode

seasonEnds = []

for i in range(len(episodesDf)-10):
    if episodesDf.iloc[i+1]['season'] != episodesDf.iloc[i]['season']:
        seasonEnds.append(i)

seasonEnds.append(153)

fig = plt.figure()
fig.set(facecolor = (0.98, 0.72, 0.14, 0.99))

plt.plot(episodesDf[["rating"]], color='gold', alpha = 0.60, linewidth = 0.75)
plt.plot(episodesDf[["ratingCount"]] / 1000, color='whitesmoke', alpha = 0.60, linewidth = 0.75)
plt.ylim(1, 10)
plt.grid(True, axis = 'y', color = 'dimgrey', alpha = 0.02)
plt.xlabel("Episode", labelpad=14, fontsize = 12)
plt.ylabel("Rating & # of user ratings / 1000", labelpad=14, fontsize = 12)
plt.title("IMDB rating and number of ratings (1000) of all IASIP episodes\n separated by seasons", 
          y=1.1, fontsize = 12)
plt.legend(['Rating', 'Number of ratings / 1000'], fontsize=8, facecolor = 'black', edgecolor = 'white',
           loc = 'center right', bbox_to_anchor=(1.15, 0.5), labelcolor = 'white',
           fancybox=True, shadow=True)

[plt.axvline(i, linewidth=0.6, color='royalblue', alpha = 0.3, linestyle = '-') for i in seasonEnds]

ax = plt.axes()
ax.set(facecolor = "black")
ax.set_alpha(0.1)

plt.show()

# index of episodes with 'dee, dennis, mac, charlie, frank' in their title

#names = ['Dee','Dennis','Charlie','Frank','Mac']

charEps = {
        # PTSDee
        'Dee' : [129],
        # D.E.N.N.I.S. system
        'Dennis' : [54],
        'Charlie' : [],
        'Frank' : [],
        'Mac' : []
    }

for i in range(len(episodesDf)):
    split = episodesDf.iloc[i]['title'].split(" ")
    for name in charEps.keys():
        if name in split or name + "'s" in split or name + "'" in split:
            charEps.get(name).append(i)

# sorting dennis' and dee's episodes because of the dennis system and ptsdee episodes

charEps.get('Dennis').sort()
charEps.get('Dee').sort()

# who has the most self-based episodes

createBarplot(charEps.keys(), [len(x) for x in charEps.values()], 
              "Character", "Number of episodes", 
              "Number of episodes with each \nmain character's name in the title", 
              [len(x) for x in charEps.values()])

# who has the best rated self-named episodes

meanRatings = [round(mean(episodesDf.loc[charEps.get(x)]['rating']), 2) for x in charEps.keys()]

createBarplot(charEps.keys(), meanRatings,
              "Character", "Average rating",
              "Average rating of episodes with the\n main characters' names in it",
              meanRatings, ylimF = 6)

# average season ratings

seasonAvg = [round(mean(episodesDf.query('season == ' + str(i+1))['rating']), 2) for i in range(14)]
seasonErr = [round(max(episodesDf.query('season == ' + str(i+1))['rating']) - 
                   mean(episodesDf.query('season == ' + str(i+1))['rating']), 2) for i in range(14)]

createBarplot([x+1 for x in range(14)], seasonAvg,
              "Season", "Average rating",
              "Average IMDb rating of each season, \nwith best episode's rating errorbar",
              seasonAvg, yerr_ = seasonErr, ylimF = 6,
              textRotate = 90, fontSBarLabel = 13)

# number of episodes per side-characters

f = open("data/sideCharacters.txt", "r")
sideCharsDict = f.read()
sideCharsDict = eval(sideCharsDict)
f.close()

createBarplot(sideCharsDict.keys(), [int(x) for x in sideCharsDict.values()], 
              "Side characters", "Number of episodes", 
              "Number of episodes these side characters appear in", 
              [int(x) for x in sideCharsDict.values()],
              textRotate = 90, fontSBarLabel = 13, 
              xTicksRotate = 90, heightBarLabel = 0.5)

# chart of views per season

# getting the middle of the seasons to place the ticks

seasonTicks = [3.5]

for i in range(len(seasonEnds)-1):
    seasonTicks.append(((seasonEnds[i+1] - seasonEnds[i]) / 2) + seasonEnds[i])

# where views are 0, fill it with the average views of that season (excluding 0)

for i in episodesDf.query("views == 0").index:
    episodesDf.at[i, 'views'] = round(episodesDf[episodesDf['season'] == episodesDf.loc[i].season].query("views != 0")[["views"]].mean()[0],2)

fig = plt.figure()
fig.set(facecolor = (0.98, 0.72, 0.14, 0.99))

plt.plot(episodesDf.query("season > 3")[["views"]], color='gold', alpha = 0.60, linewidth = 1.2)
plt.grid(True, axis = 'y', color = 'dimgrey', alpha = 0.13)
plt.xlabel("Season", labelpad=14, fontsize = 12)
plt.ylabel("Million views", labelpad=14, fontsize = 12)
plt.title("TV views (in millions) of IASIP episodes (seasons 4-14)\n separated by seasons", 
          y=1.1, fontsize = 12)

plt.xticks(ticks = seasonTicks[3:14], 
           labels = ["Season " + str(x+1) for x in range(3, 14)], 
           rotation = 60)

[plt.axvline(i, linewidth=0.6, color='w', alpha = 0.3, linestyle = '-') for i in seasonEnds[2:14]]

ax = plt.axes()
ax.set(facecolor = "black")
ax.set_alpha(0.1)

plt.show()