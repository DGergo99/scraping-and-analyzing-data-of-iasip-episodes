# -*- coding: utf-8 -*-
"""
Created on Thu Apr 15 12:31:36 2021

@author: gdvid
"""

# getting the imDb id of the series

import requests
 
f = open("data/API.txt", "r")
API_KEY = f.read()

url = "https://imdb-api.com/en/API/Search/" + API_KEY + "/It's Always Sunny in Philadelphia 2005"

payload = {}
headers= {}

response = requests.request("GET", url, headers = headers, data = payload).json()

results = response.get("results")

str_res = str(results)[1:25]

seriesId = str_res.split("'")[3]

# collecting basic info about the series, number of seasons, basic plot description, imdb and metacritic rating

url = "https://imdb-api.com/en/API/Title/" + API_KEY + "/" + seriesId

response = requests.request("GET", url, headers=headers, data = payload).json()

seriesInfo = {
    'seriesId' : seriesId,
    'numOfSeasons' : len(response.get("tvSeriesInfo").get("seasons")),
    'basePlot' : response.get("plot"),
    'imdbRating' : response.get("imDbRating"),
    'metaRating' : response.get("metacriticRating")
}

# collecting character and actor names

characterNames = []

for character in response.get("actorList"):
    charName = character.get("asCharacter")
    split = charName.split(" ")[:2]
    
    if split[0] == 'Mac':
        characterNames.append('Mac')
    else:
        characterNames.append(' '.join(split))

actorNames = {
    str(characterNames[0]) : response.get("actorList")[0].get("name"),
    str(characterNames[1]) : response.get("actorList")[1].get("name"),
    str(characterNames[2]) : response.get("actorList")[2].get("name"),
    str(characterNames[3]) : response.get("actorList")[3].get("name"),
    str(characterNames[4]) : response.get("actorList")[4].get("name")
}

# collecting data about individual episodes

import pandas as pd

episodeData = pd.DataFrame(columns = ['id', 'title', 'rating', 'ratingCount', 'season', 'episode', 'year'])

for i in range(seriesInfo.get("numOfSeasons")+1):
    
    url = "https://imdb-api.com/en/API/SeasonEpisodes/" + API_KEY + "/" + seriesId + "/" + str(i)
    
    response = requests.request("GET", url, headers=headers, data = payload).json()
     
    for element in response.get("episodes"):
        episodeData = episodeData.append({'id' : element.get("id"), 'title' : element.get("title"), 
                                          'rating' : element.get("imDbRating"), 'ratingCount' : element.get("imDbRatingCount"), 
                                          'season' : element.get("seasonNumber"), 'episode' : element.get("episodeNumber"),
                                          'year' : element.get("year")}, ignore_index = True)

# collecting the number of occurences by side character

url = "https://imdb-api.com/en/API/FullCast/" + API_KEY + "/" + seriesId

response = requests.request("GET", url, headers = headers, data = payload).json()

# indexes of important side characters range from 5-20 (0-4 is the gang)

sideChars = response.get('actors')[5:20]

sideName = []
sideEpNum = []

for character in sideChars:
    name = character.get('asCharacter')
    yearsLeft = name.split(',')[0]
    
    try:
        split = yearsLeft.split(' ')    
        split.remove('episodes')
        split.remove('/')
        split.remove('...')
    except ValueError:
        pass
    
    nameAndEps = ' '.join(split)
    
    sideName.append(nameAndEps)

# deleting my man Ernie because he only appears in one episode

del sideName[5]

# number of episodes is the last element of the name as for now, so we split the name and add it to another list

for i in range(len(sideName)):
    name = sideName[i].split(' ')[:-1]
    episodes = sideName[i].split(' ')[-1]
    
    sideName[i] = ' '.join(name)
    sideEpNum.append(episodes)

sideCharDict = dict(zip(sideName, sideEpNum))

"""

#you can't get data from individual episodes, but this loop was made to collect each episode's  writers and directors :(

for i in range(len(episodeData)):
    
    url = "https://imdb-api.com/en/API/FullCast/" + API_KEY + "/" + episodeData.iloc[i]['id']

"""

# saving the data into files

f = open("data/basic_data.txt", "w")
f.write(str(seriesInfo))

f = open("data/characters.txt", "w")
f.write(str(actorNames))

f = open("data/sideCharacters.txt", "w")
f.write(str(sideCharDict))

f.close()

episodeData.to_csv("data/episode_data.csv", sep = ',', index = False)
