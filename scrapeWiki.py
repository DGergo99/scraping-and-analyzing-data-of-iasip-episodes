# -*- coding: utf-8 -*-
"""
Created on Fri Apr 16 15:36:08 2021

@author: gdvid
"""

# scraping wikipedia for TV views data

from bs4 import BeautifulSoup
import requests

url = "https://en.wikipedia.org/wiki/List_of_It%27s_Always_Sunny_in_Philadelphia_episodes"
response = requests.get(url, timeout = 20)
wikiText = response.text

soup = BeautifulSoup(wikiText, 'html.parser')

table = soup.find_all('table', {'class' : "wikitable plainrowheaders wikiepisodetable"})

print(str(table))

import pandas as pd

returnedList = pd.read_html(str(table))

viewList = []

for i in range(len(returnedList)):
    if 'US viewers(millions)' in returnedList[i].columns:
        df = pd.DataFrame(returnedList[i]["US viewers(millions)"])
        values = df.iloc[:, 0].tolist()
        viewList.append(values)

numList = []

# in each list element we also have an "[x]" number after the views, we do not need that, so we can split each datapoint and only keep the first part (the views)

for list_ in viewList:
    for listelem in list_:
        if pd.notna(listelem):
            split = listelem.split("[")[0]
            numList.append(split)
        else:
            numList.append(0)

# using the csv file made in the other scraping script
        
episodesDf = pd.read_csv('episode_data.csv')

# there is no viewership data about the first 3 seasons, so we fill the whole column with 'nan' and then add the actual values by index

episodesDf['views'] = 'nan'

episodesDf['views'][(len(episodesDf)-len(numList)) : len(episodesDf)] = numList

episodesDf.to_csv("episode_data_wviews.csv", sep = ',', index = False)
