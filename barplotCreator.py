from matplotlib import cm
from matplotlib import pyplot as plt

def createBarplot(valueX, valueY, labelX, labelY, title, 
                  colorValues, yerr_ = None, ylimF = 1,
                  textRotate = 0, fontSBarLabel = 15,
                  xTicksRotate = 0, heightBarLabel = 0.8):
    #valueX: charEps.keys() = character names
    #valueY: charEps.values() = character episodes
    
    fig = plt.figure()
    fig.set(facecolor = 'lightgray')
    
    plt.ylim(ylimF, round(max(valueY), 1)+2)
    
    plt.xticks(rotation=xTicksRotate)

    #lengths = [len(x) for x in valueY]

    colors = cm.RdYlGn([x / float(max(colorValues) * 1.3) for x in colorValues])

    plot = plt.bar(valueX, [x for x in valueY], 
                   color = colors,
                   edgecolor = 'black')

    plt.grid(True, axis = 'y', color = 'white', alpha = 0.2)

    plt.xlabel(labelX, labelpad=14, fontsize = 12)
    plt.ylabel(labelY, labelpad=14, fontsize = 12)
    plt.title(title, y=1.1, fontsize = 14)
    
    #plotlen = len(plot)
    #print(plotlen)
    
    barIndex = 0
    
    for value in plot:
        height = value.get_height()
        
        plt.text(value.get_x() + value.get_width()/2. + 0.01,
             heightBarLabel * height,'%r' % str(height), ha='center', rotation = textRotate, 
             va='bottom', fontsize = fontSBarLabel, fontweight = 1000, color = 'black')
        
        if yerr_ is not None:
            plt.errorbar(value.get_x() + value.get_width()/2., 
                         y = height, yerr = yerr_[barIndex], ecolor = 'w',
                         capsize = 3, lolims = True, linestyle = "--")
        
        barIndex = barIndex + 1

    

    ax = plt.axes()
    ax.set(facecolor = 'black')
    
    
    plt.show()